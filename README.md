# E-UE-R weil EÜR nicht geht

### ⚠️ WARNING: README DRIVEN DEVELOPMENT

**This repository is using [Readme Driven Development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html), which means that everything you can read here are still ideas and plans for the future! The first public release will be some time in 2021.**

---

Es soll eine Webanwendung werden um über das Jahr Einnahmen und Ausgaben eintragen zu können. Es soll daraus automatisch eine Einnahmenüberschussrechnung erstellt werden. 

Dabei können auch mehrere Posten zusammengefasst werden. Zum Beispiel kann der Punkt "Mitgliedsbeiträge" unter den Einnamen eine Liste aller Mitglieder und ihrerer individuellen Beiträge sein.
Außerdem können noch nicht aus-/eingegangene Beträge erfasst werden, die in der Vorschau angezeigt werden, jedoch nicht in der EÜR.